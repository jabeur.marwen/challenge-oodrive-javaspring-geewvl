package com.odrive.vacationmanager.web.controller;

import com.odrive.vacationmanager.entity.Request;
import com.odrive.vacationmanager.entity.Status;
import com.odrive.vacationmanager.service.RequestService;
import com.odrive.vacationmanager.web.exception.RequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/requests")
public class RequestController {

    @Autowired
    private RequestService requestService;

    /**
     * find all Worker Requestes
     * @param workerId
     * @return
     * @throws RequestException
     */
    @GetMapping(value = "/worker-requests")
    public List<Request> findWorkerRequestes(@RequestParam(name = "workerId") Integer workerId) throws RequestException {
        if (workerId == null)
            throw new RequestException("workerId cant not be null");

        return requestService.findAllByAuthor_Id(workerId);
    }

    /**
     * find Worker Requestes By status
     * @param status
     * @param workerId
     * @return
     * @throws RequestException
     */
    @GetMapping(value = "/worker-requests-by-status")
    public List<Request> findWorkerRequestesBystatus(@RequestParam(name = "status",required = false) String status,
                                          @RequestParam(name = "workerId") Integer workerId) throws RequestException {
        if (workerId == null)
            throw new RequestException("workerId cant not be null");

        if (Status.fromString(status) == null)
            return requestService.findAllByAuthor_Id(workerId);

        return requestService.findAllByStatusAndAuthor_Id(status, workerId);
    }

    /**
     * find Worker Requestes By status
     * @param workerId
     * @return
     * @throws RequestException
     */
    @GetMapping(value = "/worker-requests-number")
    public int countVacationDays(@RequestParam(name = "workerId") Integer workerId) throws RequestException {
        if (workerId == null)
            throw new RequestException("workerId cant not be null");

        return requestService.countVacationDays(workerId);
    }

    /**
     * add new request
     * @param workerId
     * @return
     * @throws RequestException
     */
    @PostMapping(value = "/worker-add-request")
    public Request addRequest(@RequestParam(name = "workerId") Integer workerId, @RequestBody Request request) throws RequestException {
        if (workerId == null)
            throw new RequestException("workerId cant not be null");

        if (requestService.countVacationDays(workerId) >= 30)
            throw new RequestException("you cant add new request");

        return requestService.addRequest(request);
    }

    /**
     * find Manager Requestes By status {approved and pending}
     * @param managerId
     * @return
     * @throws RequestException
     */
    @GetMapping(value = "/manager-requests-by-status")
    public List<Request> findManagerRequestesBystatus(@RequestParam(name = "managerId") Integer managerId) throws RequestException {
        if (managerId == null)
            throw new RequestException("managerId cant not be null");

        return requestService.findByStatusAndManager_id(Status.getApprovedAndPendingStatus(), managerId);
    }

    /**
     * find Manager Worker Requestes By status {approved and pending}
     * @param managerId
     * @param workerId
     * @return
     * @throws RequestException
     */
    @GetMapping(value = "/manager-worker-requests")
    public List<Request> findManagerWorkerRequestesBystatus(@RequestParam(name = "managerId") Integer managerId,
                                                      @RequestParam(name = "workerId") Integer workerId) throws RequestException {
        if (managerId == null)
            throw new RequestException("managerId cant not be null");

        if (managerId == null)
            throw new RequestException("workerId is null");

        return requestService.findAllByAuthor_Id(workerId);
    }

    /**
     * find Manager overlapping Requestes
     * @param managerId
     * @return
     * @throws RequestException
     */
    @GetMapping(value = "/manager-overlapping-requests")
    public List<Request> findManagerOverlappingRequestes(@RequestParam(name = "managerId") Integer managerId) throws RequestException {
        if (managerId == null)
            throw new RequestException("managerId can not be null");

        return requestService.findOverlappingRequestes(managerId);
    }

    /**
     * process request
     * @param managerId
     * @return
     * @throws RequestException
     */
    @PostMapping(value = "/manager-process-request")
    public Request processRequest(@RequestParam(name = "managerId") Integer managerId,
                                  @RequestParam(name = "requestId") Integer requestId,
                                  @RequestParam(name = "statu") String statu) throws RequestException {
        if (managerId == null)
            throw new RequestException("workerId cant not be null");

        return requestService.updateRequest(requestId, statu);
    }
}
