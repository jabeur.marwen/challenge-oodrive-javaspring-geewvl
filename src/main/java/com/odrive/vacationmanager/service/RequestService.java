package com.odrive.vacationmanager.service;

import com.odrive.vacationmanager.entity.Request;
import com.odrive.vacationmanager.entity.Status;

import java.util.List;

public interface RequestService {

    /**
     * find All requests By Status And Author_Id
     * @param status
     * @param workerId
     * @return
     */
    List<Request> findAllByStatusAndAuthor_Id(String status, Integer workerId);

    /**
     * find All requests Author_Id
     * @param workerId
     * @return
     */
    List<Request> findAllByAuthor_Id(Integer workerId);

    /**
     * count number of vacation day for worker
     * @param authorId
     * @return
     */
    int countVacationDays(Integer authorId);

    /**
     * add new request
     * @param request
     * @return
     */
    Request addRequest(Request request);

    /**
     *
     * @param statuses
     * @param managerId
     * @return
     */
    List<Request> findByStatusAndManager_id(List<Status> statuses, Integer managerId);

    /**
     *
     * @param managerId
     * @return
     */
    List<Request> findOverlappingRequestes(Integer managerId);

    /**
     * update request
     * @param requestId
     * @return
     */
    Request updateRequest(Integer requestId, String statu);
}
