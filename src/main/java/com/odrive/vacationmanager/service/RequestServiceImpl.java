package com.odrive.vacationmanager.service;

import com.odrive.vacationmanager.dao.RequestDao;
import com.odrive.vacationmanager.entity.Request;
import com.odrive.vacationmanager.entity.Status;
import com.odrive.vacationmanager.web.exception.RequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class RequestServiceImpl implements RequestService{

    @Autowired
    private RequestDao requestDao;

    /**
     *
     * @param status
     * @param workerId
     * @return
     */
    @Override
    public List<Request> findAllByStatusAndAuthor_Id(String status, Integer workerId) {
        return requestDao.findAllByStatusAndAuthor_Id(Status.fromString(status), workerId);
    }

    /**
     *
     * @param workerId
     * @return
     */
    @Override
    public List<Request> findAllByAuthor_Id(Integer workerId) {
        return requestDao.findAllByAuthor_Id(workerId);
    }

    /**
     * count number of vacation day for worker
     * @param authorId
     * @return
     */
    @Override
    public int countVacationDays(Integer authorId) throws RequestException{
        Calendar calendar = new GregorianCalendar();
        int year = calendar.get(Calendar.YEAR);
        List<Request> requests = requestDao.findByYearAndAuthor_Id(year, authorId);
        Date lasteDayOfYear = null;
        try {
            lasteDayOfYear = parseStringToDate(year+"-12-31");
        } catch (Exception e) {
            throw new RequestException(e.getMessage());
        }
        int sum = 0;
        for(Request request : requests){
            Date endDate = request.getVacationEndDate();
            if(endDate.after(lasteDayOfYear))
                endDate = lasteDayOfYear;

            sum = sum + countDiffBetwTwoDay(request.getVacationStartDate(), endDate);
        }
        return sum;
    }

    /**
     * add new request
     * @param request
     * @throws RequestException
     */
    @Override
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public Request addRequest(Request request) throws RequestException {
        int countDaysOfrequests = countVacationDays(request.getAuthor().getId())
                + countDiffBetwTwoDay(request.getVacationStartDate(), request.getVacationEndDate());
        if(countDaysOfrequests > 30L)
            throw new RequestException("you have exceeded the limit");
        return requestDao.save(request);
    }

    /**
     *
     * @param statuses
     * @param managerId
     * @return
     */
    @Override
    public List<Request> findByStatusAndManager_id(List<Status> statuses, Integer managerId) {
        return requestDao.findByStatusAndManager_id(statuses, managerId);
    }

    /**
     *
     * @param managerId
     * @return
     */
    @Override
    public List<Request> findOverlappingRequestes(Integer managerId) {
        return requestDao.findOverlappingRequestes(managerId);
    }

    /**
     * update Request statu
     * @param requestId
     * @param statu
     * @return
     * @throws RequestException
     */
    @Override
    public Request updateRequest(Integer requestId, String statu) throws RequestException{
        Request request = this.findOne(requestId);
        if (request == null)
            throw new RequestException("invalid requestId");

        if (Status.fromString(statu) == null)
            throw new RequestException("invalid statu");

        request.setStatus(Status.fromString(statu));

        return requestDao.save(request);
    }

    public Request findOne(Integer id) {
        Optional<Request> o = requestDao.findById(id);
        return o.isPresent() ? o.get() : null;
    }

    /**
     * count number of day between to date
     * @param startDate
     * @param endDate
     * @return
     */
    private int countDiffBetwTwoDay(Date startDate, Date endDate){
        int diffInDays = (int) ((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24));
        return diffInDays;
    }

    /**
     * parse string to date
     * @param date
     * @return
     * @throws Exception
     */
    private Date parseStringToDate(String date) throws Exception{
        return new SimpleDateFormat("yyyy-MM-dd").parse(date);
    }
}
