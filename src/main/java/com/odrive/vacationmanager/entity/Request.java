package com.odrive.vacationmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "REQUEST")
@Getter
@Setter
public class Request {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "AUTHOR", referencedColumnName = "ID")
    private Worker author;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "RESOLVED_BY", referencedColumnName = "ID")
    private Manager resolvedBy;

    @Column(name = "REQUEST_CREATED_AT")
    @Temporal(TemporalType.DATE)
    private Date requestCreatedAt;

    @Column(name = "VACATION_START_DATE")
    @Temporal(TemporalType.DATE)
    private Date vacationStartDate;

    @Column(name = "VACATION_END_DATE")
    @Temporal(TemporalType.DATE)
    private Date vacationEndDate;

    public Request() {
    }

    public Request(Worker author, Status status, Manager resolvedBy, Date requestCreatedAt, Date vacationStartDate, Date vacationEndDate) {
        this.author = author;
        this.status = status;
        this.resolvedBy = resolvedBy;
        this.requestCreatedAt = requestCreatedAt;
        this.vacationStartDate = vacationStartDate;
        this.vacationEndDate = vacationEndDate;
    }
}
