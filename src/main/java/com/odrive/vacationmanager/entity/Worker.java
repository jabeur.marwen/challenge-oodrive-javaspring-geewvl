package com.odrive.vacationmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "WORKER")
@Getter
@Setter
public class Worker extends Employee {

    @OneToMany(mappedBy = "author")
    List<Request> requests;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "MANAGER_ID", referencedColumnName = "ID")
    private Manager manager;

    public Worker() {
    }

    public Worker(String first_name, String last_name, String career, Manager manager) {
        super(first_name, last_name, career);
        this.manager = manager;
    }
}
