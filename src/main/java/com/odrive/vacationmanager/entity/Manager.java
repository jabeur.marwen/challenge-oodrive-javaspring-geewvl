package com.odrive.vacationmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "MANAGER")
@Getter
@Setter
public class Manager extends Employee {

    @OneToMany(mappedBy = "manager")
    List<Worker> workers;

    public Manager() {
    }

    public Manager(String first_name, String last_name, String career) {
        super(first_name, last_name, career);
    }
}
