package com.odrive.vacationmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@MappedSuperclass
public abstract class Employee {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "FIRST_NAME")
    String first_name;

    @Column(name = "LAST_NAME")
    String last_name;

    @Column(name = "CAREER")
    String career;

    public Employee() {
    }

    public Employee(String first_name, String last_name, String career) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.career = career;
    }
}
