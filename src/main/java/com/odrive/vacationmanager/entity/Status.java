package com.odrive.vacationmanager.entity;


import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

public enum Status {
    APPROVED("approved"),
    REJECTED("rejected"),
    PENDING("pending");

    private String status;
    Status(String s) {
        this.status = s;
    }

    public static List<Status> getApprovedAndPendingStatus(){
        return Arrays.asList(new Status[]{APPROVED,PENDING});
    }

    public String getStatus() {
        return status;
    }

    public static Status fromString(String name) {
        if (StringUtils.isNotBlank(name))
            for (Status status : Status.values()) {
                if (status.getStatus().equalsIgnoreCase(name)) {
                    return status;
                }
            }
        return null;
    }
}
