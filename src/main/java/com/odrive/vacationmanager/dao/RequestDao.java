package com.odrive.vacationmanager.dao;

import com.odrive.vacationmanager.entity.Request;
import com.odrive.vacationmanager.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestDao extends JpaRepository<Request, Integer> {

    List<Request> findAllByStatusAndAuthor_Id(Status status, Integer workerId);

    List<Request> findAllByAuthor_Id(Integer workerId);

    @Query("SELECT request " +
            "FROM Request request " +
            "LEFT JOIN request.author author " +
            "WHERE author.id = :authorId " +
            "AND YEAR(request.vacationStartDate) = :year")
    List<Request> findByYearAndAuthor_Id(@Param("year") int year,@Param("authorId") Integer authorId);

    @Query("SELECT request " +
            "FROM Request request " +
            "LEFT JOIN request.author author " +
            "WHERE author.manager = :managerId " +
            "AND request.status In (:statuses)")
    List<Request> findByStatusAndManager_id(List<Status> statuses, Integer managerId);

    @Query("SELECT request1 " +
            "FROM Request request1 " +
            "INNER JOIN Request request2 " +
            "ON request2.vacationStartDate >= request1.vacationStartDate " +
            "AND request2.vacationStartDate <= request1.vacationEndDate " +
            "LEFT JOIN request1.author author " +
            "WHERE author.manager = :managerId")
   List<Request> findOverlappingRequestes(Integer managerId);
}
