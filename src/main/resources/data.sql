DROP TABLE IF EXISTS REQUEST;
DROP TABLE IF EXISTS WORKER;
DROP TABLE IF EXISTS MANAGER;
-- -----------------------------------------------------
-- Table `vacation-manager`.`MANAGER`
-- -----------------------------------------------------

CREATE TABLE MANAGER (
  ID INT AUTO_INCREMENT  PRIMARY KEY,
  FIRST_NAME VARCHAR(250) NOT NULL,
  LAST_NAME VARCHAR(250) NOT NULL,
  CAREER VARCHAR(250) DEFAULT NULL
);

-- -----------------------------------------------------
-- Table `vacation-manager`.`WORKER`
-- -----------------------------------------------------

CREATE TABLE WORKER (
  ID INT AUTO_INCREMENT  PRIMARY KEY,
  FIRST_NAME VARCHAR(250) NOT NULL,
  LAST_NAME VARCHAR(250) NOT NULL,
  CAREER VARCHAR(250) DEFAULT NULL,
  MANAGER_ID INT NOT NULL,
  FOREIGN KEY (MANAGER_ID) REFERENCES MANAGER(ID)
);

-- -----------------------------------------------------
-- Table `vacation-manager`.`REQUEST`
-- -----------------------------------------------------

CREATE TABLE REQUEST (
  ID INT AUTO_INCREMENT  PRIMARY KEY,
  AUTHOR INT NOT NULL,
  STATUS VARCHAR (255),
  RESOLVED_BY INT NOT NULL,
  REQUEST_CREATED_AT DATE NOT NULL,
  VACATION_START_DATE DATE NOT NULL,
  VACATION_END_DATE DATE DEFAULT NULL,
  FOREIGN KEY (AUTHOR) REFERENCES WORKER(ID),
  FOREIGN KEY (RESOLVED_BY) REFERENCES MANAGER(ID)
);
