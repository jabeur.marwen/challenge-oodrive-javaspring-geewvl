package com.odrive.vacationmanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.odrive.vacationmanager.entity.Manager;
import com.odrive.vacationmanager.entity.Request;
import com.odrive.vacationmanager.entity.Status;
import com.odrive.vacationmanager.entity.Worker;
import com.odrive.vacationmanager.service.RequestService;
import com.odrive.vacationmanager.web.controller.RequestController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RequestServiceTest {

    @Autowired
    RequestService requestService;

    @Test
    public void addRequestTest() throws Exception{
        List<Request> requests = buildRequest();
        Request request = requestService.addRequest(requests.get(0));
        assertNotNull(request);
    }

    @Test
    public void findAllByStatusAndAuthor_IdTest() throws Exception{
        List<Request> requests = buildRequest();
        Request request1 = requestService.addRequest(requests.get(0));
        requests = requestService.findAllByStatusAndAuthor_Id("PENDING", requests.get(0).getId());
        assertNotNull(requests);
    }

    @Test
    public void findAllByAuthor_IdTest() throws Exception {
        List<Request> requests = buildRequest();
        Request request1 = requestService.addRequest(requests.get(0));
        requests = requestService.findAllByAuthor_Id(requests.get(0).getAuthor().getId());
        assertNotNull(requests);
    }

    @Test
    public void countVacationDaysTest() throws Exception {
        List<Request> requests = buildRequest();
        Request request1 = requestService.addRequest(requests.get(0));
        int n = requestService.countVacationDays(requests.get(0).getAuthor().getId());
        assertNotNull(n);
    }

    @Test
    public void findByStatusAndManager_idTest() throws Exception {

    }

    @Test
    public void findOverlappingRequestesTest() throws Exception {
        List<Request> requests = buildRequest();
        Request request1 = requestService.addRequest(requests.get(0));
        Request request2 = requestService.addRequest(requests.get(1));
        requests = requestService.findOverlappingRequestes(requests.get(0).getAuthor().getId());
        assertNotNull(requests);
    }
    @Test
    public void updateRequestTest() throws Exception {
        List<Request> requests = buildRequest();
        Request request1 = requestService.addRequest(requests.get(0));
        Request request2 = requestService.addRequest(requests.get(1));
        Request request = requestService.updateRequest(requests.get(0).getId(),"approved");
        assertNotNull(request);
        assertEquals(request.getStatus(),Status.fromString("approved"));
        request = requestService.updateRequest(requests.get(0).getId(),"rejected");
        assertNotNull(request);
        assertEquals(request.getStatus(),Status.fromString("rejected"));
    }

    private List<Manager> buildManager() {
        Manager e1 = new Manager("Manager 1", "Manager", "Senior developer");
        List<Manager> managers = Arrays.asList(e1);
        return managers;
    }

    private List<Worker> buildWorker() {
        List<Manager> managers = buildManager();
        Worker e1 = new Worker("Worker 1", "Worker", "developer", managers.get(0));
        Worker e2 = new Worker("Worker 2", "Worker", "developer", managers.get(0));
        List<Worker> workers = Arrays.asList(e1, e2);
        return workers;
    }

    private List<Request> buildRequest() {
        List<Worker> workers = buildWorker();
        Request e1 = new Request(workers.get(0), Status.PENDING, workers.get(0).getManager(), new Date(),
                new Date(2021,12,15), new Date(2022,1,5));
        Request e2 = new Request(workers.get(1), Status.PENDING, workers.get(1).getManager(), new Date(),
                new Date(2021,12,15), new Date(2022,1,10));
        List<Request> requests = Arrays.asList(e1, e2);
        return requests;
    }
}
